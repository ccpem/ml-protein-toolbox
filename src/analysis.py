import math
import os

import numpy as np
import pandas as pd
import tensorflow as tf
from scipy import ndimage
from sklearn.model_selection import train_test_split
from tensorflow.keras import Model, backend
from tensorflow.keras.layers import (
    Activation,
    BatchNormalization,
    Conv3D,
    Conv3DTranspose,
    Dropout,
    Input,
    LeakyReLU,
    MaxPooling3D,
    PReLU,
    UpSampling3D,
    concatenate,
)
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.regularizers import l1, l1_l2, l2
from tensorflow.keras.utils import to_categorical
from tensorflow.python.ops import clip_ops, math_ops

from src.proteins import Sample
from src.utils import progress
from src.viewer import confusion_matrix, plot_metric


def class_occurence(
    data,
    mode="penal_weight",
    labs=None,
    per_sample=False,
    appendix="",
    verbose=False,
    debug=True,
):
    modes = ["nums", "props", "plain_weight", "penal_weight"]

    if str(type(data)) == "<class 'tensorflow.python.framework.ops.EagerTensor'>":
        ys = data.numpy()
    elif str(type(data)) == "<class 'numpy.ndarray'>":
        if debug:
            if not all((y % int(y)) == 0 for y in np.unique(data)):
                raise RuntimeError(
                    "When calculating proportions, np.array must contain integers only."
                )
            else:
                ys = data
        else:
            ys = data
    elif (
        isinstance(data, list) and str(type(data[0])) == "<class 'src.proteins.Sample'>"
    ):
        if not data[0].has_tiles:
            ys = np.asarray([d.lab for d in data])
        else:
            # if tiles present need to count in tiles as maps are not the same size (will fail in unique)
            # this will overestimate the counts due to overlaps between tiles
            ys = np.asarray([ltile for d in data for ltile in d.ltiles])
    else:
        raise RuntimeError(
            "Parameter 'data' should be either a np.array, a list or tf.Tensor type."
        )

    if labs is None:
        vals, count = np.unique(ys, return_counts=True)
    else:
        vals = np.asarray(labs)
        count = np.array([np.size(ys[ys == u]) for u in vals])

    if (
        per_sample
        and str(type(data)) == "<class 'tensorflow.python.framework.ops.EagerTensor'>"
    ):
        raise RuntimeError(
            "Parameter 'per_sample' is only supported if data is an instance of 'Sample' class."
        )
    if per_sample:
        if verbose:
            print(
                "\n-------------------------- CLASS PROPORTIONS PER CLASS {}:".format(
                    appendix
                )
            )
        ids = [d.id for d in data]
        # _, count = list(zip(*[np.unique(y, return_counts=True) for y in ys]))
        # print(count)
        c_count = np.array([[np.size(y[y == u]) for u in vals] for y in ys])
        c_props = (
            c_count / np.reshape(np.sum(c_count, axis=1), (len(c_count), 1))
        ) * 100
        props_df = pd.DataFrame(c_props, index=ids, columns=vals.astype(int)).round(3)
        if verbose:
            print(props_df)

    props = count / np.sum(count)
    if mode == "nums":
        if verbose:
            print("\n-------------------------- CLASS OCCURRENCES {}:".format(appendix))
        ret = dict(zip(vals.astype(int), count))
    elif mode == "props":
        if verbose:
            print("\n-------------------------- CLASS PROPORTIONS {}:".format(appendix))
        ret = dict(zip(vals.astype(int), props * 100))
    elif mode == "plain_weight":
        if verbose:
            print(
                "\n-------------------------- CLASS WEIGHTS (SIMPLE) {}:".format(
                    appendix
                )
            )
        ret = (1 - props) / np.sum(1 - props)
        ret = dict(zip(vals.astype(int), ret))
    elif mode == "penal_weight":
        if verbose:
            print(
                "\n-------------------------- CLASS WEIGHTS (PENALISE) {}:".format(
                    appendix
                )
            )
        ret = np.nan_to_num((1 / count) / np.sum(1 / count[np.isfinite(1 / count)]))
        ret = dict(zip(vals.astype(int), ret))
    else:
        raise RuntimeError("Mode must be one of {}".format(modes))

    ret_df = pd.DataFrame.from_dict(ret, orient="index", columns=[mode]).round(3)
    if verbose:
        print(ret_df)

    return ret_df.values.T[0]


def unet_small(
    inpt,
    labs,
    up=False,
    feats=False,
    batchnorm=False,
    activ=None,
    l1_value=None,
    l2_value=None,
    dropout_rate=0.0,
):
    if activ is None:
        activ = "relu"
    advanced_activations = ["leakyrelu", "prelu"]
    adv_classes = [LeakyReLU, PReLU]
    adv_class = None
    if activ in advanced_activations:
        idx = advanced_activations.index(activ)
        adv_class = adv_classes[idx]

    reg = None
    if l1_value is not None:
        if l2_value is not None:
            reg = l1_l2(l1_value, l2_value)
        else:
            reg = l1(l1_value)
    else:
        if l2_value is not None:
            reg = l2(l2_value)

    conv1_1 = Conv3D(
        32,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_1",
    )(inpt)
    if batchnorm:
        conv1_1 = BatchNormalization()(conv1_1)
    if adv_class is None:
        relu1_1 = Activation(activ, name="activation_1")(conv1_1)
    else:
        relu1_1 = adv_class(name="activation_1")(conv1_1)
    conv1_2 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_2",
    )(relu1_1)
    if batchnorm:
        conv1_2 = BatchNormalization()(conv1_2)
    if adv_class is None:
        relu1_2 = Activation(activ, name="activation_2")(conv1_2)
    else:
        relu1_2 = adv_class(name="activation_2")(conv1_2)
    dropout1 = Dropout(dropout_rate)(relu1_2)
    pool1 = MaxPooling3D(pool_size=(2, 2, 2), name="max_pooling3d_1")(dropout1)

    conv2_1 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_3",
    )(pool1)
    if batchnorm:
        conv2_1 = BatchNormalization()(conv2_1)
    if adv_class is None:
        relu2_1 = Activation(activ, name="activation_3")(conv2_1)
    else:
        relu2_1 = adv_class(name="activation_3")(conv2_1)
    conv2_2 = Conv3D(
        128,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_4",
    )(relu2_1)
    if batchnorm:
        conv2_2 = BatchNormalization()(conv2_2)
    if adv_class is None:
        relu2_2 = Activation(activ, name="activation_4")(conv2_2)
    else:
        relu2_2 = adv_class(name="activation_4")(conv2_2)
    dropout2 = Dropout(dropout_rate)(relu2_2)

    if not up:
        up3 = Conv3DTranspose(
            128, (2, 2, 2), padding="same", strides=(2, 2, 2), name="conv3d_transpose_3"
        )(dropout2)
    else:
        up3 = UpSampling3D((2, 2, 2))(dropout2)
    add3 = concatenate([up3, dropout1], axis=1, name="concatenate_3")
    conv3_1 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_5",
    )(add3)
    if batchnorm:
        conv3_1 = BatchNormalization()(conv3_1)
    if adv_class is None:
        relu3_1 = Activation(activ, name="activation_5")(conv3_1)
    else:
        relu3_1 = adv_class(name="activation_5")(conv3_1)
    conv3_2 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_6",
    )(relu3_1)
    if batchnorm:
        conv3_2 = BatchNormalization()(conv3_2)
    if adv_class is None:
        relu3_2 = Activation(activ, name="activation_6")(conv3_2)
    else:
        relu3_2 = adv_class(name="activation_6")(conv3_2)

    if not feats:
        last_layer = Conv3D(labs, (1, 1, 1), name="conv3d_15")(relu3_2)
    else:
        last_layer = relu3_2
    return last_layer


def unet_medium(
    inpt,
    labs,
    up=False,
    feats=False,
    batchnorm=False,
    activ=None,
    l1_value=None,
    l2_value=None,
    dropout_rate=0.0,
):
    if activ is None:
        activ = "relu"
    advanced_activations = ["leakyrelu", "prelu"]
    adv_classes = [LeakyReLU, PReLU]
    adv_class = None
    if activ in advanced_activations:
        idx = advanced_activations.index(activ)
        adv_class = adv_classes[idx]

    reg = None
    if l1_value is not None:
        if l2_value is not None:
            reg = l1_l2(l1_value, l2_value)
        else:
            reg = l1(l1_value)
    else:
        if l2_value is not None:
            reg = l2(l2_value)

    conv1_1 = Conv3D(
        32,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_1",
    )(inpt)
    if batchnorm:
        conv1_1 = BatchNormalization()(conv1_1)
    if adv_class is None:
        relu1_1 = Activation(activ, name="activation_1")(conv1_1)
    else:
        relu1_1 = adv_class(name="activation_1")(conv1_1)
    conv1_2 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_2",
    )(relu1_1)
    if batchnorm:
        conv1_2 = BatchNormalization()(conv1_2)
    if adv_class is None:
        relu1_2 = Activation(activ, name="activation_2")(conv1_2)
    else:
        relu1_2 = adv_class(name="activation_2")(conv1_2)
    dropout1 = Dropout(dropout_rate)(relu1_2)
    pool1 = MaxPooling3D(pool_size=(2, 2, 2), name="max_pooling3d_1")(dropout1)

    conv2_1 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_3",
    )(pool1)
    if batchnorm:
        conv2_1 = BatchNormalization()(conv2_1)
    if adv_class is None:
        relu2_1 = Activation(activ, name="activation_3")(conv2_1)
    else:
        relu2_1 = adv_class(name="activation_3")(conv2_1)
    conv2_2 = Conv3D(
        128,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_4",
    )(relu2_1)
    if batchnorm:
        conv2_2 = BatchNormalization()(conv2_2)
    if adv_class is None:
        relu2_2 = Activation(activ, name="activation_4")(conv2_2)
    else:
        relu2_2 = adv_class(name="activation_4")(conv2_2)
    dropout2 = Dropout(dropout_rate)(relu2_2)
    pool2 = MaxPooling3D(pool_size=(2, 2, 2), name="max_pooling3d_2")(dropout2)

    conv3_1 = Conv3D(
        128,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_5",
    )(pool2)
    if batchnorm:
        conv3_1 = BatchNormalization()(conv3_1)
    if adv_class is None:
        relu3_1 = Activation(activ, name="activation_5")(conv3_1)
    else:
        relu3_1 = adv_class(name="activation_5")(conv3_1)
    conv3_2 = Conv3D(
        256,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_6",
    )(relu3_1)
    if batchnorm:
        conv3_2 = BatchNormalization()(conv3_2)
    if adv_class is None:
        relu3_2 = Activation(activ, name="activation_6")(conv3_2)
    else:
        relu3_2 = adv_class(name="activation_6")(conv3_2)
    dropout3 = Dropout(dropout_rate)(relu3_2)

    if not up:
        up4 = Conv3DTranspose(
            256, (2, 2, 2), padding="same", strides=(2, 2, 2), name="conv3d_transpose_1"
        )(dropout3)
    else:
        up4 = UpSampling3D((2, 2, 2))(dropout3)
    add4 = concatenate([up4, dropout2], axis=1, name="concatenate_1")
    conv4_1 = Conv3D(
        128,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_7",
    )(add4)
    if batchnorm:
        conv4_1 = BatchNormalization()(conv4_1)
    if adv_class is None:
        relu4_1 = Activation(activ, name="activation_7")(conv4_1)
    else:
        relu4_1 = adv_class(name="activation_7")(conv4_1)
    conv4_2 = Conv3D(
        128,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_8",
    )(relu4_1)
    if batchnorm:
        conv4_2 = BatchNormalization()(conv4_2)
    if adv_class is None:
        relu4_2 = Activation(activ, name="activation_8")(conv4_2)
    else:
        relu4_2 = adv_class(name="activation_8")(conv4_2)
    dropout4 = Dropout(dropout_rate)(relu4_2)

    if not up:
        up5 = Conv3DTranspose(
            128, (2, 2, 2), padding="same", strides=(2, 2, 2), name="conv3d_transpose_2"
        )(dropout4)
    else:
        up5 = UpSampling3D((2, 2, 2))(dropout4)
    add5 = concatenate([up5, dropout2], axis=1, name="concatenate_2")
    conv5_1 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_9",
    )(add5)
    if batchnorm:
        conv5_1 = BatchNormalization()(conv5_1)
    if adv_class is None:
        relu5_1 = Activation(activ, name="activation_9")(conv5_1)
    else:
        relu5_1 = adv_class(name="activation_9")(conv5_1)
    conv5_2 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_10",
    )(relu5_1)
    if batchnorm:
        conv5_2 = BatchNormalization()(conv5_2)
    if adv_class is None:
        relu5_2 = Activation(activ, name="activation_10")(conv5_2)
    else:
        relu5_2 = adv_class(name="activation_10")(conv5_2)

    if not feats:
        last_layer = Conv3D(labs, (1, 1, 1), name="conv3d_11")(relu5_2)
    else:
        last_layer = relu5_2
    return last_layer


def unet_large(
    inpt,
    labs,
    up=False,
    feats=False,
    batchnorm=False,
    activ=None,
    l1_value=None,
    l2_value=None,
    dropout_rate=0.0,
):
    if activ is None:
        activ = "relu"
    advanced_activations = ["leakyrelu", "prelu"]
    adv_classes = [LeakyReLU, PReLU]
    adv_class = None
    kws = {}
    if activ in advanced_activations:
        idx = advanced_activations.index(activ)
        adv_class = adv_classes[idx]
        if activ == "leakyrelu":
            kws = {"alpha": 0.1}

    reg = None
    if l1_value is not None:
        if l2_value is not None:
            reg = l1_l2(l1_value, l2_value)
        else:
            reg = l1(l1_value)
    else:
        if l2_value is not None:
            reg = l2(l2_value)

    conv1_1 = Conv3D(
        32,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_1",
    )(inpt)
    if batchnorm:
        conv1_1 = BatchNormalization()(conv1_1)
    if adv_class is None:
        relu1_1 = Activation(activ, name="activation_1")(conv1_1)
    else:
        relu1_1 = adv_class(name="activation_1", **kws)(conv1_1)
    conv1_2 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_2",
    )(relu1_1)
    if batchnorm:
        conv1_2 = BatchNormalization()(conv1_2)
    if adv_class is None:
        relu1_2 = Activation(activ, name="activation_2")(conv1_2)
    else:
        relu1_2 = adv_class(name="activation_2", **kws)(conv1_2)
    dropout1 = Dropout(dropout_rate)(relu1_2)
    pool1 = MaxPooling3D(pool_size=(2, 2, 2), name="max_pooling3d_1")(dropout1)

    conv2_1 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_3",
    )(pool1)
    if batchnorm:
        conv2_1 = BatchNormalization()(conv2_1)
    if adv_class is None:
        relu2_1 = Activation(activ, name="activation_3")(conv2_1)
    else:
        relu2_1 = adv_class(name="activation_3", **kws)(conv2_1)
    conv2_2 = Conv3D(
        128,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_4",
    )(relu2_1)
    if batchnorm:
        conv2_2 = BatchNormalization()(conv2_2)
    if adv_class is None:
        relu2_2 = Activation(activ, name="activation_4")(conv2_2)
    else:
        relu2_2 = adv_class(name="activation_4", **kws)(conv2_2)
    dropout2 = Dropout(dropout_rate)(relu2_2)
    pool2 = MaxPooling3D(pool_size=(2, 2, 2), name="max_pooling3d_2")(dropout2)

    conv3_1 = Conv3D(
        128,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_5",
    )(pool2)
    if batchnorm:
        conv3_1 = BatchNormalization()(conv3_1)
    if adv_class is None:
        relu3_1 = Activation(activ, name="activation_5")(conv3_1)
    else:
        relu3_1 = adv_class(name="activation_5", **kws)(conv3_1)
    conv3_2 = Conv3D(
        256,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_6",
    )(relu3_1)
    if batchnorm:
        conv3_2 = BatchNormalization()(conv3_2)
    if adv_class is None:
        relu3_2 = Activation(activ, name="activation_6")(conv3_2)
    else:
        relu3_2 = adv_class(name="activation_6", **kws)(conv3_2)
    dropout3 = Dropout(dropout_rate)(relu3_2)
    pool3 = MaxPooling3D(pool_size=(2, 2, 2), name="max_pooling3d_3")(dropout3)

    conv4_1 = Conv3D(
        256,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_7",
    )(pool3)
    if batchnorm:
        conv4_1 = BatchNormalization()(conv4_1)
    if adv_class is None:
        relu4_1 = Activation(activ, name="activation_7")(conv4_1)
    else:
        relu4_1 = adv_class(name="activation_7", **kws)(conv4_1)
    conv4_2 = Conv3D(
        512,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_8",
    )(relu4_1)
    if batchnorm:
        conv4_2 = BatchNormalization()(conv4_2)
    if adv_class is None:
        relu4_2 = Activation(activ, name="activation_8")(conv4_2)
    else:
        relu4_2 = adv_class(name="activation_8", **kws)(conv4_2)
    dropout4 = Dropout(dropout_rate)(relu4_2)

    if not up:
        up5 = Conv3DTranspose(
            512, (2, 2, 2), padding="same", strides=(2, 2, 2), name="conv3d_transpose_1"
        )(dropout4)
    else:
        up5 = UpSampling3D((2, 2, 2))(dropout4)
    add5 = concatenate([up5, dropout3], name="concatenate_1")
    conv5_1 = Conv3D(
        256,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_9",
    )(add5)
    if batchnorm:
        conv5_1 = BatchNormalization()(conv5_1)
    if adv_class is None:
        relu5_1 = Activation(activ, name="activation_9")(conv5_1)
    else:
        relu5_1 = adv_class(name="activation_9", **kws)(conv5_1)
    conv5_2 = Conv3D(
        256,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_10",
    )(relu5_1)
    if batchnorm:
        conv5_2 = BatchNormalization()(conv5_2)
    if adv_class is None:
        relu5_2 = Activation(activ, name="activation_10")(conv5_2)
    else:
        relu5_2 = adv_class(name="activation_10", **kws)(conv5_2)
    dropout5 = Dropout(dropout_rate)(relu5_2)

    if not up:
        up6 = Conv3DTranspose(
            256,
            (2, 2, 2),
            padding="same",
            strides=(2, 2, 2),
            kernel_regularizer=reg,
            name="conv3d_transpose_2",
        )(dropout5)
    else:
        up6 = UpSampling3D((2, 2, 2))(dropout5)
    add6 = concatenate([up6, dropout2], name="concatenate_2")
    conv6_1 = Conv3D(
        128,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_11",
    )(add6)
    if batchnorm:
        conv6_1 = BatchNormalization()(conv6_1)
    if adv_class is None:
        relu6_1 = Activation(activ, name="activation_11")(conv6_1)
    else:
        relu6_1 = adv_class(name="activation_11", **kws)(conv6_1)
    conv6_2 = Conv3D(
        128,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_12",
    )(relu6_1)
    if batchnorm:
        conv6_2 = BatchNormalization()(conv6_2)
    if adv_class is None:
        relu6_2 = Activation(activ, name="activation_12")(conv6_2)
    else:
        relu6_2 = adv_class(name="activation_12", **kws)(conv6_2)
    dropout6 = Dropout(dropout_rate)(relu6_2)

    if not up:
        up7 = Conv3DTranspose(
            128,
            (2, 2, 2),
            padding="same",
            strides=(2, 2, 2),
            kernel_regularizer=reg,
            name="conv3d_transpose_3",
        )(dropout6)
    else:
        up7 = UpSampling3D((2, 2, 2))(dropout6)
    add7 = concatenate([up7, dropout1], name="concatenate_3")  # up7,relu1_2
    conv7_1 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_13",
    )(add7)
    if batchnorm:
        conv7_1 = BatchNormalization()(conv7_1)
    if adv_class is None:
        relu7_1 = Activation(activ, name="activation_13")(conv7_1)
    else:
        relu7_1 = adv_class(name="activation_13", **kws)(conv7_1)
    conv7_2 = Conv3D(
        64,
        (3, 3, 3),
        padding="same",
        strides=(1, 1, 1),
        kernel_regularizer=reg,
        name="conv3d_14",
    )(relu7_1)
    if batchnorm:
        conv7_2 = BatchNormalization()(conv7_2)
    if adv_class is None:
        relu7_2 = Activation(activ, name="activation_14")(conv7_2)
    else:
        relu7_2 = adv_class(name="activation_14", **kws)(conv7_2)

    if not feats:
        last_layer = Conv3D(labs, (1, 1, 1), name="conv3d_15")(relu7_2)
    else:
        last_layer = relu7_2
    return last_layer


def unet(
    input_shape,
    labels,
    channels=1,
    arch_mode="large",
    loss_mode="sparse_cce",
    custom_loss_weights=None,
    loadweights=False,
    batchnorm=False,
    l1=None,
    l2=None,
    activation_function=None,
    dropout_rate=0.0,
    verbose=False,
):
    arch_modes = ["large", "medium", "small"]
    loss_modes = ["scce", "weighted_scce", "focal", "prob_scce", "weighted_prob_scce"]
    activation_modes = ["relu", "tanh", "leakyrelu", "prelu"]

    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
    backend.set_image_data_format("channels_last")
    input_shape = (*input_shape, channels)

    inpt = Input(input_shape, name="input_1")

    if activation_function not in activation_modes:
        raise RuntimeError(
            "Parameter 'activation_function' must be one of: {}".format(
                activation_modes
            )
        )

    if arch_mode == "large":
        last_layer = unet_large(
            inpt,
            len(labels),
            activ=activation_function,
            l1_value=l1,
            l2_value=l2,
            batchnorm=batchnorm,
            dropout_rate=dropout_rate,
        )
    elif arch_mode == "medium":
        last_layer = unet_medium(
            inpt,
            len(labels),
            activ=activation_function,
            l1_value=l1,
            l2_value=l2,
            batchnorm=batchnorm,
            dropout_rate=dropout_rate,
        )
    elif arch_mode == "small":
        last_layer = unet_small(
            inpt,
            len(labels),
            activ=activation_function,
            l1_value=l1,
            l2_value=l2,
            batchnorm=batchnorm,
            dropout_rate=dropout_rate,
        )
    else:
        raise RuntimeError(
            "Parameter 'arch_mode' must be one of: {}".format(arch_modes)
        )

    if loss_mode == "scce":
        out = Activation("softmax", name="activation_last")(last_layer)
        loss = "sparse_categorical_crossentropy"
    elif loss_mode == "weighted_scce":
        out = Activation("softmax", name="activation_last")(last_layer)
        loss = weighted_sparse_categorical_crossentropy(labels)
    elif loss_mode == "focal":
        out = Activation("softmax", name="activation_last")(last_layer)
        loss = focal_loss()
    elif loss_mode == "prob_scce":
        out = Activation("sigmoid", name="activation_last")(last_layer)
        loss = "sparse_categorical_crossentropy"
    elif loss_mode == "weighted_prob_scce":
        out = Activation("sigmoid", name="activation_last")(last_layer)
        loss = weighted_sparse_categorical_crossentropy(labels)
    elif loss_mode == "custom_weighted_scce":
        if custom_loss_weights is None:
            raise RuntimeError(
                "You need to pass 'custom_loss_weights' parameter when using 'custom_weighted_scce'."
            )
        out = Activation("softmax", name="activation_last")(last_layer)
        loss = custom_weighted_sparse_categorical_cross_entropy(custom_loss_weights)
    else:
        raise RuntimeError(
            "Parameter 'loss_mode' must be one of: {}".format(loss_modes)
        )

    non_zero_tp = NonZeroCategoricalTruePositives()

    # TODO: Add switchable optimizers, learning rates and learning rate decays
    optimizer = Adam(learning_rate=0.01)  # 1 OOM over default

    model = Model(inputs=inpt, outputs=out)
    model.compile(
        optimizer=optimizer,
        loss=loss,
        metrics=[
            "accuracy",
            "sparse_categorical_crossentropy",
            "sparse_categorical_accuracy",
            non_zero_tp,
        ],
        run_eagerly=True,
    )

    if loadweights:
        model.load_weights(loadweights)

    if verbose:
        model.summary(line_length=150)
        for layer in model.layers:
            print(layer.name, layer.output_shape)

    return model


class NonZeroCategoricalTruePositives(tf.keras.metrics.Metric):
    def __init__(self, name="non_zero_categorical_true_positives", **kwargs):
        super(NonZeroCategoricalTruePositives, self).__init__(name=name, **kwargs)
        self.true_positives = self.add_weight(name="tp", initializer="zeros")
        self.sizes = self.add_weight(name="tp", initializer="zeros")

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_true = tf.squeeze(tf.reshape(tf.cast(y_true, "float32"), shape=(-1, 1)))
        y_pred = tf.squeeze(
            tf.reshape(tf.cast(tf.argmax(y_pred, axis=-1), "float32"), shape=(-1, 1))
        )
        mask = y_true != 0
        vals = y_pred[mask] == y_true[mask]
        vals = tf.cast(vals, "float32")
        self.true_positives.assign_add(tf.reduce_sum(vals))
        self.sizes.assign_add(tf.reduce_sum(tf.cast(mask, "float32")))

    def result(self):
        return self.true_positives / self.sizes

    def reset_states(self):
        self.true_positives.assign(0.0)
        self.sizes.assign(0.0)


def weighted_sparse_categorical_crossentropy(classes, mode=None):
    def wscc(target, output):
        loss = tf.keras.backend.sparse_categorical_crossentropy(target, output)

        if mode is not None:
            ws = class_occurence(target, labs=classes, mode=mode)
        else:
            ws = class_occurence(target, labs=classes)
        t = np.copy(target)
        weights = np.zeros(target.shape, dtype=np.float32)

        for i, u in enumerate(classes):
            weights[t == u] = ws[i]

        weights = weights.reshape(loss.shape)
        return loss * weights

    return wscc


def custom_weighted_sparse_categorical_cross_entropy(weights):
    def cwscc(target, output):
        loss = tf.keras.backend.sparse_categorical_crossentropy(target, output)
        if loss.shape != weights.shape:
            raise RuntimeError(
                "The shape of the weights {} must be the same as the "
                "shape of the error (loss) matrix {}".format(weights.shape, loss.shape)
            )
        return loss * weights

    return cwscc


def focal_loss(alpha=0.25, gamma=2):
    def focal(target, output):
        loss = tf.keras.backend.sparse_categorical_crossentropy(target, output)

        eps = tf.constant(backend.epsilon())  # non-zero
        t = to_categorical(
            target, num_classes=output.shape[-1]
        )  # from sparse to categorical
        o = output / math_ops.reduce_sum(output)  # normalise
        o = clip_ops.clip_by_value(o, eps, 1 - eps)  # clip to non-zero
        weights = math_ops.reduce_sum(alpha * np.power(1 - o, gamma) * t, axis=-1)

        return loss * weights

    return focal


def augument(xs, ys, ws=None, margin=0.01):
    """
    Auguments the dataset to within margin of the most frequent class. Augumentation
    ignores the tiles with natural occurrence less than current occurence (it would otherwise
    increase other class proportions).

    Since augumentation is slow, the code will look for a pre-saved augumentation data.
    Augumentation data will be saved once ran a single time. Be sure to remove any old
    logs/augumentation/ files.

    Augumentation stats: "Augumenting data with label {a} ({b}|{c}|{d} {e}/{f}/{g}):"
    a - label
    b - starting label proportion
    c - current label proportion
    d - target label proportion (max label frequence in the dataset)
    e - tile number (sorted by containing the most to the least proportion containing current class
    f - rotation number of the current tile (1-9)
    g - actual proportion of the current label in the current tile (will be ignored if the
        current proportion reaches more than current tile


    :param xs: (np.ndarray) training set
    :param ys: (np.ndarray) training labels
    :param ws: (np.ndarray) defaults to None, weights if available
    :param margin: (float) margin to which the augumentation is performed within
    :return: xs_aug (np.ndarray) augumented data for training set
             ys_aug (np.ndarray) corresponding labels to augumented training set
             ws_aug (np.ndarray) if ws is not None, corresponding weights to augumented training set
    """

    if ys is None:
        raise RuntimeError(
            "Can only augument training data, please pass labels for param ys."
        )

    # aug_start = len(ys)

    classes = np.unique(ys)

    if os.path.exists("logs/augumentation/xs_aug.npy"):
        xs_aug = np.load("logs/augumentation/xs_aug.npy")
        ys_aug = np.load("logs/augumentation/ys_aug.npy")
        ws_aug = np.load("logs/augumentation/ws_aug.npy")
        class_occurence(
            np.concatenate((np.array(ys), np.array(ys_aug))),
            mode="props",
            verbose=True,
            labs=classes,
        )
        return xs_aug, ys_aug, ws_aug

    props_per_tile = []
    total = len(ys) * len(classes)
    for y in ys:
        props = []
        for c in classes:
            props.append([np.count_nonzero(y == c)])
            progress(
                (len(props_per_tile) * len(classes) + c),
                total,
                "Gathering per-tile proportions",
            )
        props_per_tile.append(props)

    totals_per_class = np.sum(props_per_tile, axis=0)
    fs = totals_per_class / np.sum(totals_per_class)
    fs = fs[:, 0]
    # freqs  class_occurence(np.array(ys), mode='props')[1:]
    freqs = list(zip(fs[1:], classes[1:]))
    # freqs = [f for f in enumerate(freqs)]
    freqs.sort(key=lambda x: x[0], reverse=True)

    max_freq = freqs[0][0]
    lowbound = max_freq - (margin * max_freq)
    highbound = max_freq + (margin * max_freq)
    underrepresented = [
        f[0] for f in freqs if not lowbound <= f[0] <= highbound
    ]  # within 10% of max
    if len(underrepresented) == 0:
        # labels are balanced
        return xs, ys, ws

    xs_aug, ys_aug, ws_aug = ([], [], [])
    aug_totals_per_class = np.zeros_like(totals_per_class)

    # freqs = reversed(freqs)

    for f, lab in freqs:
        print("Label", lab, ":", f)

        # for each label add data if underrepresented
        if f >= lowbound:
            print(
                "Label {} is within {}% of the most frequent class.\n".format(
                    lab, margin * 100
                )
            )
            continue  # ignore if it's the most frequent label

        # sort tile in order of containing the most amount of the current label l
        tile_props = list(
            zip(
                [
                    np.sum(y == lab) / np.size(y)
                    for y in ys
                    if np.sum(y == lab) / np.size(y) > 0
                ],
                range(0, len(ys)),
            )
        )
        tile_props.sort(key=lambda x: x[0], reverse=True)

        prop = f
        for t, (tileprop, tileid) in enumerate(tile_props):
            # print(f, prop, max_freq, lowbound)
            # print(tileprop, max_freq)
            if tileprop < prop:
                # ignore tiles with natural proportion lower than we're aiming at (it will increase other classes and lower this class)
                print()
                print(
                    "\nWARNING: cannot increase label {} anymore, no more tiles with proportion of more than current "
                    "{} available.\n".format(lab, prop)
                )
                break
            if prop > lowbound:
                break
            for i in range(3):
                for j in range(3):
                    ys_aug.append(randrot90(ys[tileid], ax=i, tm=j))
                    xs_aug.append(randrot90(xs[tileid], ax=i, tm=j))
                    if ws is not None:
                        ws_aug.append(randrot90(ws[tileid], ax=i, tm=j))
                    # prop = class_occurence(np.concatenate((ys, ys_aug)), mode='props')[l]
                    # tile proportions are definitionally the same, since the voxels are identical, so sum that
                    aug_totals_per_class = (
                        aug_totals_per_class + props_per_tile[tileid][lab]
                    )
                    prop = (totals_per_class[lab] + aug_totals_per_class[lab]) / (
                        np.sum(totals_per_class) + np.sum(aug_totals_per_class)
                    )
                    prop = prop.item()
                    progress(
                        int(((prop - f) / (max_freq - f) * 100)),
                        100,
                        desc="Augumenting data with label {} ({:.2f}|{:.2f}|{:.2f} {}/{}/{:.2f})".format(
                            lab,
                            f,
                            prop,
                            max_freq,
                            t + 1,
                            (i * 3) + j + 1,
                            tileprop * 100,
                        ),
                    )
                    if prop > lowbound:
                        print()
                        print(
                            "\nWARNING: label has been augumented to within {}% of the most frequent label, "
                            "stopping.\n".format(margin * 100)
                        )
                        break
                if prop > lowbound:
                    break
            if tileid == len(tile_props):
                print()
                print("\nWARNING: no more data to augument for label {}".format(lab))

    class_occurence(
        np.concatenate((np.array(ys), np.array(ys_aug))), mode="props", verbose=True
    )

    path = "logs/augumentation"
    if not os.path.exists(path):
        os.makedirs(path)
    np.save(os.path.join(path, "xs_aug.npy"), xs_aug)
    np.save(os.path.join(path, "ys_aug.npy"), ys_aug)
    np.save(os.path.join(path, "ws_aug.npy"), ws_aug)
    return xs_aug, ys_aug, ws_aug


def randrot90(data, ax=None, tm=None, retall=False):
    """
    Random or controlled rotation (if ax and tm params provided).

    :param data: (np.ndarray) 3d volume
    :param ax: (int) 0 for yaw, 1 for pitch, 2 for roll
    :param tm: (int) number of times to rotate, between <1 and 3>
    :param retall: (bool) if True, will parameters of the rotation (ax, tm)
    :return:
    """
    import random

    rotations = [(0, 1), (0, 2), (1, 2)]  # yaw, pitch, roll
    if ax is None and tm is None:
        axes = random.randint(0, 2)
        times = random.randint(1, 3)
    elif ax is None or tm is None:
        raise RuntimeError(
            "When specifying rotation, please use both arguments to specify the axis and number of times to rotate."
        )
    else:
        axes = ax
        times = tm
    d = np.rot90(data, times, rotations[axes])
    if retall:
        return d, ax, tm
    return d


def randrot(data, ax=None, an=None, retall=False, interp=True):
    """
    Random or controlled rotation (if ax and tm params provided).

    :param data: (np.ndarray) 3d volume
    :param ax: (int) 0 for yaw, 1 for pitch, 2 for roll
    :param tm: (int) number of times to rotate, between <1 and 3>
    :param retall: (bool) if True, will parameters of the rotation (ax, tm)
    :return:
    """

    import random

    rotations = [(0, 1), (0, 2), (1, 2)]  # yaw, pitch, roll

    if ax is None and an is None:
        axes = random.randint(0, 2)
        set_angles = [30, 60, 90]
        angler = random.randint(0, 2)
        angle = set_angles[angler]

    elif an is None or ax is None:
        raise RuntimeError(
            "When specifying rotation, please use both arguments to specify the axis and angle."
        )
    else:
        axes = ax
        angle = an
    r = rotations[axes]

    d = ndimage.rotate(
        data, angle, axes=r, reshape=False, order=(3 if interp else 0)
    )  # 0 means nearest neighbour, which we want for labelling
    if retall:
        return d, ax, angle

    return d


class PlotMetrics(tf.keras.callbacks.Callback):
    def __init__(self, x_test=None, y_test=None, ts=False):
        super(PlotMetrics).__init__()
        self.x_test = x_test
        self.y_test = y_test
        self.ts = ts
        self.loss, self.acc, self.tp = ([], [], [])

    def on_epoch_end(self, epoch, logs=None):
        loss = "sparse_categorical_crossentropy"
        loss_val = "val_sparse_categorical_crossentropy"
        acc = "sparse_categorical_accuracy"
        acc_val = "val_sparse_categorical_accuracy"
        tp = "non_zero_categorical_true_positives"
        tp_val = "val_non_zero_categorical_true_positives"

        # if self.x_test is not None and self.y_test is not None:
        #     ev = dict(zip(self.model.metrics_names, self.model.evaluate(self.x_test, self.y_test, verbose=0)))
        #     loss_tst, acc_tst, tp_tst = (ev[loss], ev[acc], ev[tp])
        #     self.loss.append(loss_tst)
        #     self.acc.append(acc_tst)
        #     self.tp.append(tp_tst)
        #     loss_tst, acc_tst, tp_tst = (self.loss, self.acc, self.tp)
        # else:
        #     loss_tst, acc_tst, tp_tst = (None, None, None)

        if epoch >= 1:
            x = np.arange(0, epoch + 1) + 1

            loss_t = [*self.model.history.history[loss], logs.get(loss)]
            loss_v = [*self.model.history.history[loss_val], logs.get(loss_val)]

            acc_t = [*self.model.history.history[acc], logs.get(acc)]
            acc_v = [*self.model.history.history[acc_val], logs.get(acc_val)]
            y_max = len(acc_v) - np.argmax(acc_v[::-1])
            plot_metric(
                x,
                loss_t,
                loss_v,
                acc_t,
                acc_v,
                # loss_tst=loss_tst, acc_tst=acc_tst,
                title="Training and Validation Loss and Accuracy | Best: epoch {}".format(
                    y_max
                ),
                save="accuracy.png",
                colour_scheme="blues",
                ts=self.ts,
            )

            tp_t = [*self.model.history.history[tp], logs.get(tp)]
            tp_v = [*self.model.history.history[tp_val], logs.get(tp_val)]
            y_max = len(tp_v) - np.argmax(tp_v[::-1])
            plot_metric(
                x,
                loss_t,
                loss_v,
                tp_t,
                tp_v,
                # loss_tst=loss_tst, acc_tst=tp_tst,
                title="Training and Validation Loss and True Positives | Best: epoch {}".format(
                    y_max
                ),
                save="tp.png",
                colour_scheme="greens",
                mname="Non-zero TP",
                ts=self.ts,
            )


class PlotConfusion(tf.keras.callbacks.Callback):
    def __init__(
        self,
        labels,
        x_val,
        y_val,
        x_test=None,
        y_test=None,
        x_train=None,
        y_train=None,
        maps=None,
        ts=False,
        lab_names=None,
    ):
        super(PlotConfusion).__init__()
        self.x_val = x_val
        self.y_val = y_val
        self.x_test = x_test
        self.y_test = y_test
        self.x_train = x_train
        self.y_train = y_train
        self.labels = labels
        self.lab_names = lab_names
        self.sample = maps
        self.ts = ts

    def on_epoch_end(self, epoch, logs=None):
        y_pred_val = np.argmax(self.model.predict(self.x_val), axis=-1)
        confusion_matrix(
            self.y_val,
            y_pred_val.ravel(),
            self.labels,
            lab_names=self.lab_names,
            save="epoch{:04d}_val.png".format(epoch + 1),
            title="Validation Confusion Matrix | Epoch: {}".format(epoch + 1),
            ts=self.ts,
        )
        if self.x_test is not None and self.y_test is not None:
            y_pred_test = np.argmax(self.model.predict(self.x_test), axis=-1).ravel()
            confusion_matrix(
                self.y_test,
                y_pred_test,
                self.labels,
                lab_names=self.lab_names,
                save="epoch{:04d}_tst.png".format(epoch + 1),
                title="Testing Confusion Matrix | Epoch: {}".format(epoch + 1),
                ts=self.ts,
            )
        if self.x_train is not None and self.y_train is not None:
            y_pred_train = np.argmax(self.model.predict(self.x_train), axis=-1).ravel()
            confusion_matrix(
                self.y_train,
                y_pred_train,
                self.labels,
                lab_names=self.lab_names,
                save="epoch{:04d}_trn.png".format(epoch + 1),
                title="Training Confusion Matrix | Epoch: {}".format(epoch + 1),
                ts=self.ts,
            )

        if self.sample is not None:
            print(
                "\nWARNING: Saving validation predictions, sample {}.".format(
                    self.sample.id
                )
            )
            if self.sample.has_tiles:
                im = y_pred_val[0 : self.sample.no_tiles]
            else:
                im = y_pred_val[0]
            self.sample.pred = im
            self.sample.save_pred()


def train_unet(
    data,
    lab_names=None,
    epochs=50,
    batch=1,
    arch_mode="large",
    loss_mode="scce",
    patience=30,
    querymaps=False,
    verbose=False,
    callbacks=None,
    get_all_preds=False,
    aug=False,
    l1=None,
    l2=None,
    activation_mode=None,
    dropout_rate=0.0,
    batchnorm=False,
):
    # TODO paralelise this
    if not str(type(data[0])) == "<class 'src.proteins.Sample'>":
        raise RuntimeError(
            "Data must be of type src.protein.Sample, not {}.".format(type(data[0]))
        )

    # assign train/val/test set to each data point ONLY IF NOT ALREADY ASSIGNED
    if data[0].mode is None:
        d_train, d_test = train_test_split(data, test_size=0.1)  # 90% / 10%
        d_train, d_val = train_test_split(
            d_train, test_size=0.2 / (1 - 0.1)
        )  # 70% / 20%
        for d in data:
            if d in d_train:
                d.set_train()
            elif d in d_val:
                d.set_val()
            elif d in d_test:
                d.set_test()
    else:
        d_train = [d for d in data if d.mode == Sample.Mode.TRAIN]
        d_val = [d for d in data if d.mode == Sample.Mode.VAL]
        d_test = [d for d in data if d.mode == Sample.Mode.TEST]

    print("Validating:", [(d.res, d.id) for d in d_val])
    print("Testing:", [(d.res, d.id) for d in d_test])
    print(
        "\n-------------------------- DATA SPLIT:\ntrain: {}\n  val: {}\n test: {}".format(
            len(d_train), len(d_val), len(d_test)
        )
    )

    class_occurence(
        d_train, mode="props", per_sample=querymaps, appendix="train", verbose=True
    )
    class_occurence(
        d_val, mode="props", per_sample=querymaps, appendix="val", verbose=True
    )
    class_occurence(
        d_test, mode="props", per_sample=querymaps, appendix="test", verbose=True
    )

    # augument if tiles
    if not data[0].has_tiles and aug:
        raise RuntimeError(
            "Cannot augument the data if not used in tiles preprocessing mode."
        )

    if loss_mode == "custom_weighted_scce":
        aug_start, x_train, x_val, x_test, y_train, y_val, y_test, w_train = (
            reshape_data(d_train, d_val, d_test, weights=True, aug=aug)
        )
    else:
        aug_start, x_train, x_val, x_test, y_train, y_val, y_test = reshape_data(
            d_train, d_val, d_test, aug=aug
        )
        w_train = None

    labels = np.unique(np.concatenate((y_train, y_val, y_test)))

    dshape = data[0].map.shape if not data[0].has_tiles else data[0].tiles[0].shape
    mshape = str(dshape[0]) + "x" + str(dshape[1]) + "x" + str(dshape[2])
    mname = "model_{}".format(mshape)
    model = unet(
        x_train.shape[1:-1],
        labels,
        channels=x_train.shape[-1],
        arch_mode=arch_mode,
        loss_mode=loss_mode,
        custom_loss_weights=w_train,
        verbose=verbose,
        l1=l1,
        l2=l2,
        dropout_rate=dropout_rate,
        activation_function=activation_mode,
        batchnorm=batchnorm,
    )

    if callbacks is None:
        callbacks = [
            "checkpoints",
            "metrics",
            "confusion",
            "maps",
            "stopping",
            "tests",
        ]  # , 'trains', 'timestamp']
    cbs = []
    if "checkpoints" in callbacks:
        if not os.path.exists("logs/checkpoints"):
            os.makedirs("logs/checkpoints")
        cbs.append(
            tf.keras.callbacks.ModelCheckpoint(
                os.path.join(
                    "logs/checkpoints", mname + "_epoch{epoch:04d}_checkpoint.h5"
                ),
                save_weights_only=False,
            )
        )
    if "stopping" in callbacks:
        cbs.append(tf.keras.callbacks.EarlyStopping(monitor="loss", patience=patience))
    if "metrics" in callbacks:
        tests = (x_test, y_test) if "tests" in callbacks else (None, None)
        ts = "timestamp" in callbacks
        cbs.append(PlotMetrics(x_test=tests[0], y_test=tests[1], ts=ts))
    if "confusion" in callbacks:
        maps = d_val[0] if "maps" in callbacks else None
        tests = (x_test, y_test) if "tests" in callbacks else (None, None)
        trains = (x_train, y_train) if "trains" in callbacks else (None, None)
        ts = "timestamp" in callbacks
        cbs.append(
            PlotConfusion(
                labels,
                x_val,
                y_val,
                x_test=tests[0],
                y_test=tests[1],
                x_train=trains[0],
                y_train=trains[1],
                maps=maps,
                ts=ts,
                lab_names=lab_names,
            )
        )

    print("\n-------------------------- TRAINING BEGINS")
    print(
        "input: {} | arch: {} | loss: {} | epochs: {} | batch: {}\n".format(
            x_train.shape[1:-1], arch_mode, loss_mode, epochs, batch
        )
    )
    model.fit(
        x=x_train,
        y=y_train,
        validation_data=(x_val, y_val),
        epochs=epochs,
        batch_size=batch,  # verbose=2,
        callbacks=cbs,
    )
    if aug:
        x_train = x_train[:aug_start]
        y_train = y_train[:aug_start]
        if w_train is not None:
            w_train = w_train[:aug_start]

    # model.save_weights(os.path.join('checkpoints', mname+'_final_weights.h5'))
    # model.save(os.path.join('checkpoints', mname + datetime.now().strftime("_%Y%m%d-%H%M%S")+'.h5'))
    model.save(os.path.join("logs/checkpoints", mname + "_final.h5"))

    print("\n-------------------------- TRAINING ENDS")
    print()
    print("\n-------------------------- GENERATING PREDICTIONS")

    if get_all_preds:
        all_preds = predict_unet(
            data, model_name=os.path.join("logs/checkpoints", mname + "_final.h5")
        )
        return all_preds
    preds = predict_unet(
        d_test, model_name=os.path.join("logs/checkpoints", mname + "_final.h5")
    )
    return preds


def predict_unet(
    data,
    model_name="model_64x64x64_final.h5",
    batch=1,
    evaluate=False,
    querymaps=False,
    verbose=False,
):
    if not os.path.exists(model_name):
        if len(model_name.split("/")) <= 1:
            print(
                '\nWARNING: Model {} does not exist, searching available models in "logs/checkpoints" instead.'.format(
                    model_name
                )
            )

            if (
                not os.path.exists("logs/checkpoints")
                or len(os.listdir("logs/checkpoints")) == 0
            ):
                raise RuntimeError("No trained models available.")

            if model_name not in os.listdir("logs/checkpoints"):
                print(
                    '\nWARNING: Model {} does not exist in directory "logs/checkpoints". Chosing the last saved checkpoint instead'.format(
                        model_name
                    )
                )
                model_name = sorted(os.listdir("logs/checkpoints"))[-1]

            model_name = "logs/checkpoints/" + model_name

        else:
            raise RuntimeError("\nModel {} does not exist.".format(model_name))

    if not str(type(data[0])) == "<class 'src.proteins.Sample'>":
        raise RuntimeError(
            "Data must be of type src.protein.Sample, not {}.".format(type(data[0]))
        )

    if len(data[0].map.shape) != 3:
        raise RuntimeError(
            "Data must be 3D in and in the dimension the model was trained on."
        )

    model = tf.keras.models.load_model(model_name, compile=False)
    #                                       custom_objects={'wscc': weighted_sparse_categorical_crossentropy(ydata)})

    mshape = model.layers[0]._batch_input_shape[1:-1]
    dshape = data[0].map.shape if not data[0].has_tiles else data[0].tiles[0].shape
    if mshape != dshape:
        raise RuntimeError(
            "This model was trained on images with following dimensions: {}. You are attempting to "
            "predict images of dimension {}. Please make sure you preprocess your data to an "
            "appropriate shape.\n\nThis is likely to be a result of pre-processing, try using"
            " --cshape flag along with your preprocessing option to specify the desired shape of your"
            " data.".format(mshape, dshape)
        )

    if evaluate:
        class_occurence(
            data, mode="props", per_sample=querymaps, appendix="pred_eval", verbose=True
        )
        # ydata = np.expand_dims([d.lab for d in data], axis=-1)
        _, xdata, ydata = reshape_data(data)
    # xdata = np.expand_dims([d.map for d in data], axis=-1)
    else:
        _, xdata = reshape_data(data, eval=False)

    preds = model.predict(xdata, batch_size=batch)
    preds = tf.argmax(preds, axis=-1).numpy()
    if evaluate:
        model.evaluate(xdata, ydata, batch_size=batch)

    step = 0
    for i in range(len(data)):
        if data[0].has_tiles:
            data[i].pred = preds[step : step + data[i].no_tiles]
            step += data[i].no_tiles
        else:
            data[i].pred = preds[i]
        data[i].save_pred()
        if evaluate:
            data[i].save_diff()

    return preds


def reshape_data(d_train, d_val=None, d_test=None, eval=True, weights=False, aug=False):
    """Reshape Sample data for training in tensorflow. Will return tiles is they exist instead of map."""

    channels = False
    aug_start = len(d_train)  # used to keep track of where augumentation begins

    if not d_train[0].has_tiles:
        if len(d_train[0].map.shape) == 4:
            channels = True

        x_train = (
            np.expand_dims([d.map for d in d_train], axis=-1)
            if not channels
            else np.array([d.map for d in d_train])
        )

        if eval:
            y_train = np.expand_dims([d.lab for d in d_train], axis=-1)
        if d_val is not None:
            x_val = (
                np.expand_dims([d.map for d in d_val], axis=-1)
                if not channels
                else np.array([d.map for d in d_val])
            )
            if eval:
                y_val = np.expand_dims([d.lab for d in d_val], axis=-1)
        if d_test is not None:
            x_test = (
                np.expand_dims([d.map for d in d_test], axis=-1)
                if not channels
                else np.array([d.map for d in d_test])
            )
            if eval:
                y_test = np.expand_dims([d.lab for d in d_test], axis=-1)
        if weights:
            w_train = np.asarray([d.weight for d in d_train])
    else:
        if len(d_train[0].tiles[0].shape) == 4:
            channels = True

        x_train = [tile for d in d_train for tile in d.tiles]
        y_train = None
        w_train = None
        if eval:
            y_train = [tile for d in d_train for tile in d.ltiles]
        if weights:
            w_train = [tile for d in d_train for tile in d.wtiles]
        if aug:
            # augument data
            xs, ys, ws = augument(x_train, y_train, ws=w_train)
            x_train.extend(xs)
            y_train.extend(ys)
            if weights:
                w_train.extend(ws)

        if not channels:
            x_train = np.expand_dims(x_train, axis=-1)
        else:
            x_train = np.asarray(x_train)
        x_train = x_train.reshape(-1, *x_train.shape[-4:])

        if eval:
            y_train = np.expand_dims(y_train, axis=-1)
            y_train = y_train.reshape(-1, *y_train.shape[-4:])
        if d_val is not None:
            x_val = (
                np.expand_dims([tile for d in d_val for tile in d.tiles], axis=-1)
                if not channels
                else np.array([tile for d in d_val for tile in d.tiles])
            )
            x_val = x_val.reshape(-1, *x_val.shape[-4:])
            if eval:
                y_val = np.expand_dims(
                    [tile for d in d_val for tile in d.ltiles], axis=-1
                )
                y_val = y_val.reshape(-1, *y_val.shape[-4:])
        if d_test is not None:
            x_test = (
                np.expand_dims([tile for d in d_test for tile in d.tiles], axis=-1)
                if not channels
                else np.array([tile for d in d_test for tile in d.tiles])
            )
            x_test = x_test.reshape(-1, *x_test.shape[-4:])
            if eval:
                y_test = np.expand_dims(
                    [tile for d in d_test for tile in d.ltiles], axis=-1
                )
                y_test = y_test.reshape(-1, *y_test.shape[-4:])
        if weights:
            w_train = np.asarray(w_train)
            w_train = w_train.reshape(-1, *w_train.shape[-4:])

    ret = [aug_start, x_train]
    if d_val:
        ret.append(x_val)
    if d_test:
        ret.append(x_test)
    if eval:
        ret.append(y_train)
        ret.append(y_val)
        ret.append(y_test)
    if weights:
        ret.append(w_train)
    return tuple(ret)


def patch_nd(img, scale=3, ravel=True, zeros=False):
    """
    Default method for feature extraction. Simple n-dimensional dense patch extraction.
    """

    feats = []
    shape = img.shape

    for n in range(img.size):
        # work out the index of current pixel/voxel; base-n conversion
        indices = []
        prev = n
        for i in reversed(range(len(shape))):
            indices.append(int(prev % img.shape[i]))
            prev = prev / img.shape[i]

        # work out shapes of slices
        slices = []
        for i, ind in enumerate(reversed(indices)):
            # from array of starts and stops for x y and z
            start = ind  # index of cropped array minus half length of patch
            stop = (
                ind + scale
            )  # index of cropped array minus half length of patch plus patch length
            slices.append(slice(start, stop))

        # slice window of size PATCH from index position
        feature = img[slices]
        if feature.size != int(math.pow(scale, len(shape))):
            # reject edge features
            continue
        if ravel:
            feature = np.asarray(feature.ravel())
        feats.append(feature)

    feats = np.asarray(feats)

    if feats.shape[0] != np.prod((np.array(shape) - (scale // 2 * 2))):
        # print(feats.shape)
        # print(np.prod((np.array(shape) - (scale // 2 * 2))))
        # check the size of the final feature space
        raise RuntimeError(
            "Error in feature extraction. Check if patch_size is an odd number!"
        )

    if zeros:
        # TODO refactor this so that it is placed in the matrix of np.zeros((*shape, f_dim))
        margin = scale // 2
        f_dim = feats.shape[-1]
        new_s = shape + (f_dim,)
        feats_s = tuple(np.array(shape) - (scale // 2 * 2)) + (f_dim,)

        new_feats = np.zeros(new_s, dtype=feats.dtype)
        feats = feats.reshape(feats_s)

        crop = []
        for d in range(len(shape)):
            crop.append(slice(margin, -margin))

        new_feats[crop] = feats
        new_feats = new_feats.reshape(np.prod(new_feats.shape[:-1]), f_dim)
        return new_feats
    return np.asarray(feats)
