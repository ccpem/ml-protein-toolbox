# Important notes:
#   - The class occurence function is not yet supported.
#   - Remember when to use logits and when to use predictions.
import enum
import os
import sys
from typing import List, Union

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as F

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))  # noqa: E402
from util.decorators import check_prediction  # noqa: E402


class Loss(enum.Enum):
    CrossEntropyLoss = "CrossEntropyLoss"
    FocalLoss = "FocalLoss"
    DiceLoss = "DiceLoss"
    NLLLoss = "NLLLoss"
    TverskyLoss = "TverskyLoss"
    WeightedSparseCategoricalCrossentropy = "WSSC"
    MultiLoss = "MultiLoss"


class Reduction(enum.Enum):
    mean = "mean"
    sum_ = "sum"
    none = "none"


class LossNotYetSupportedException(Exception):
    def __init__(self, loss: Loss):
        super().__init__(
            f"Chosen loss {loss} is not yet supported. Please chose a loss from the list of supported "
            f"losses: {list(Loss.__members__.keys())}"
        )


class ReductionNotYetSupportedException(Exception):
    def __init__(self, reduction: Reduction):
        super().__init__(
            f"Chosen reduction {reduction} is not yet supported. Please chose a reduction from the list of supported "
            f"reductions: {list(Reduction.__members__.keys())}"
        )


# class instead of value error when giving logts and not predictions
class NotBetweenZeroAndOneError(Exception):
    """
    Exception raised when values are not between 0 and 1.
    This could be because the values are logits and not predictions.
    """

    def __init__(self):
        super().__init__(
            "Values given are not between 0 and 1. This could be because the values are logits and not predictions."
        )


class CrossEntropyLoss(nn.Module):
    def __init__(self, reduction: Union[str, Reduction] = Reduction.mean.value):
        super(CrossEntropyLoss, self).__init__()
        if isinstance(reduction, str):
            reduction = Reduction(reduction)
        self.reduction = reduction.value

    def forward(self, logits, target):
        # add check for one hot encoding or class labels
        return nn.CrossEntropyLoss(reduction=self.reduction)(logits, target)


class FocalLoss(nn.Module):
    def __init__(
        self,
        alpha: float = 0.25,
        gamma: float = 2,
        reduction: Union[str, Reduction] = Reduction.mean,
    ):
        super(FocalLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma
        if isinstance(reduction, str):
            reduction = Reduction(reduction)
        self.reduction = reduction.value

    def forward(self, logits, target):
        loss = nn.CrossEntropyLoss(reduction="none")(logits, target)
        eps = torch.tensor(1e-7)
        t = F.one_hot(target.long(), num_classes=logits.shape[-1])
        o = logits / torch.sum(logits)
        o = torch.clamp(o, eps, 1 - eps)
        weights = torch.sum(self.alpha * torch.pow(1 - o, self.gamma) * t, dim=-1)
        focal_loss = weights * loss

        if self.reduction == "mean":
            return focal_loss.mean()
        elif self.reduction == "sum":
            return focal_loss.sum()
        else:  # 'none'
            return focal_loss


class DiceLoss(nn.Module):
    def __init__(self, smooth=1):
        super(DiceLoss, self).__init__()
        self.smooth = smooth

    @check_prediction
    def forward(self, prediction, target_one_hot):
        # https://www.kaggle.com/code/bigironsphere/loss-function-library-keras-pytorch
        prediction = prediction.view(-1)
        target_one_hot = target_one_hot.view(-1)
        intersection = (prediction * target_one_hot).sum()
        dice = (2.0 * intersection + self.smooth) / (
            prediction.sum() + target_one_hot.sum() + self.smooth
        )
        return 1 - dice


class NLLLoss(nn.Module):
    def __init__(self, reduction: Union[str, Reduction] = Reduction.mean):
        super(NLLLoss, self).__init__()
        if isinstance(reduction, str):
            reduction = Reduction(reduction)
        self.reduction = reduction.value

    def forward(self, logits, target):
        # https://www.youtube.com/watch?v=Ni1ViB1Ezjs
        log_softmax = nn.LogSoftmax(dim=-1)
        return nn.NLLLoss(reduction=self.reduction)(log_softmax(logits), target)


class TverskyLoss(nn.Module):
    def __init__(self, alpha=0.5, beta=0.5):
        super(TverskyLoss, self).__init__()
        self.alpha = alpha
        self.beta = beta

    @check_prediction
    def forward(self, prediction, target_one_hot):
        smooth = 1.0
        prediction = prediction.view(-1)
        target_one_hot = target_one_hot.view(-1)
        intersection = (prediction * target_one_hot).sum()
        fp = ((1 - prediction) * prediction).sum()
        fn = (target_one_hot * (1 - target_one_hot)).sum()
        tversky = (intersection + smooth) / (
            intersection + self.alpha * fp + self.beta * fn + smooth
        )

        return 1 - tversky


class WeightedSparseCategoricalCrossentropy(nn.Module):
    """
    docstring for WeightedSparseCategoricalCrossentropy

    Notes:
        - I have removed the class occurence function from being called within
        the forward method. For now it's becuase I'm not sure if class occurence
        will work. Ultimatly, we need decide to prompt the user to use class occurence
        outside of the model and then pass the weights in as a parameter to the
        model or use it in forward.
    """

    def __init__(
        self,
        classes: List[int],
        weights: List[float],
        reduction: Union[str, Reduction] = Reduction.mean,
    ):
        super(WeightedSparseCategoricalCrossentropy, self).__init__()
        self.classes = classes
        self.weights = weights
        if isinstance(reduction, str):
            reduction = Reduction(reduction)
        self.reduction = reduction.value

    def forward(self, logits, target):
        loss = nn.CrossEntropyLoss(reduction="none")(logits, target)

        t = target.clone().detach().numpy()
        ws = np.zeros(target.shape, dtype=np.float32)
        if len(self.weights) != len(self.classes):
            raise ValueError(
                "Number of classes and weights should be the same. Got "
                f"{len(self.classes)} classes and {len(self.weights)} weights."
            )
        for i, u in enumerate(self.classes):
            ws[t == u] = self.weights[i]
        ws = ws.reshape(loss.shape)
        return loss * ws


class MultiLoss(nn.Module):
    def __init__(
        self,
        losses: List[nn.Module],
        weights: List[float],
        **kwargs,
    ):
        super(MultiLoss, self).__init__()
        self.losses = losses
        self.weights = weights
        self.weights = self.weights if self.weights is not None else self.even_weights()
        self.classes = kwargs.get("classes", None)
        self.wscc_weights = kwargs.get("wscc_weights", None)

    def even_weights(self):
        return [1.0] * len(self.losses)

    def forward(self, prediction_logits, target):
        loss = 0
        for i, loss in enumerate(self.losses):
            if isinstance(loss, WeightedSparseCategoricalCrossentropy):
                if self.wscc_weights is None:
                    raise ValueError(
                        "WeightedSparseCategoricalCrossentropy requires weights to be passed in."
                    )
                if self.classes is None:
                    raise ValueError(
                        "WeightedSparseCategoricalCrossentropy requires classes to be passed in."
                    )
                loss += self.weights[i] * loss(
                    prediction_logits,
                    target,
                    weights=self.wscc_weights,
                    classes=self.classes,
                )
            else:
                loss += self.weights[i] * loss(prediction_logits, target)

        return loss


def class_occurence(
    data,
    mode="penal_weight",
    labs=None,
    per_sample=False,
    appendix="",
    verbose=False,
    debug=True,
):
    """
    Calculate class occurrences or class proportions based on the given data.

    Parameters:
    - data: The prediction data. It should be either a np.array, a list, or a torch.Tensor type.
    - mode: The mode to calculate the class occurrences or class proportions. It should be one of ["nums", "props", "plain_weight", "penal_weight"].
    - labs: The labels to consider for calculating class occurrences or class proportions. If None, all unique labels in the data will be considered.
    - per_sample: Whether to calculate class proportions per sample. This parameter is only supported if data is an instance of 'Sample' class.
    - appendix: An optional string to append to the prediction for better identification.
    - verbose: Whether to print the calculated class occurrences or class proportions.
    - debug: Whether to enable debug mode.

    Returns:
    - A numpy array containing the calculated class occurrences or class proportions.

    Raises:
    - RuntimeError: If the prediction data is not of the expected type or if the mode is not one of the supported modes.
    - RuntimeError: If the 'per_sample' parameter is True and the data is not an instance of 'Sample' class.
    - RuntimeError: If the prediction data contains non-integer values when calculating proportions.

    Note:
    - The function supports different modes to calculate class occurrences or class proportions:
        - "nums": Calculate class occurrences.
        - "props": Calculate class proportions.
        - "plain_weight": Calculate class weights using a simple formula.
        - "penal_weight": Calculate class weights using a penalizing formula.
    """

    modes = ["nums", "props", "plain_weight", "penal_weight"]

    if isinstance(data, torch.Tensor):
        ys = data.numpy()
    elif isinstance(data, np.ndarray):
        if debug:
            if not all((y % int(y)) == 0 for y in np.unique(data)):
                raise RuntimeError(
                    "When calculating proportions, np.array must contain integers only."
                )
            else:
                ys = data
        else:
            ys = data
    # Need to explore switching from src.proteins to ccpem parsers
    # elif isinstance(data, list) and isinstance(data[0], Sample):
    #     if not data[0].has_tiles:
    #         ys = np.asarray([d.lab for d in data])
    #     else:
    #         ys = np.asarray([ltile for d in data for ltile in d.ltiles])
    else:
        raise RuntimeError(
            "Parameter 'data' should be either a np.array, a list or torch.Tensor type."
        )

    if labs is None:
        vals, count = np.unique(ys, return_counts=True)
    else:
        vals = np.asarray(labs)
        count = np.array([np.size(ys[ys == u]) for u in vals])

    if per_sample and isinstance(data, torch.Tensor):
        raise RuntimeError(
            "Parameter 'per_sample' is only supported if data is an instance of 'Sample' class."
        )
    if per_sample:
        if verbose:
            print(
                "\n-------------------------- CLASS PROPORTIONS PER CLASS {}:".format(
                    appendix
                )
            )
        ids = [d.id for d in data]
        c_count = np.array([[np.size(y[y == u]) for u in vals] for y in ys])
        c_props = (
            c_count / np.reshape(np.sum(c_count, axis=1), (len(c_count), 1))
        ) * 100
        props_df = pd.DataFrame(c_props, index=ids, columns=vals.astype(int)).round(3)
        if verbose:
            print(props_df)

    props = count / np.sum(count)
    if mode == "nums":
        if verbose:
            print("\n-------------------------- CLASS OCCURRENCES {}:".format(appendix))
        ret = dict(zip(vals.astype(int), count))
    elif mode == "props":
        if verbose:
            print("\n-------------------------- CLASS PROPORTIONS {}:".format(appendix))
        ret = dict(zip(vals.astype(int), props * 100))
    elif mode == "plain_weight":
        if verbose:
            print(
                "\n-------------------------- CLASS WEIGHTS (SIMPLE) {}:".format(
                    appendix
                )
            )
        ret = (1 - props) / np.sum(1 - props)
        ret = dict(zip(vals.astype(int), ret))
    elif mode == "penal_weight":
        if verbose:
            print(
                "\n-------------------------- CLASS WEIGHTS (PENALISE) {}:".format(
                    appendix
                )
            )
        ret = np.nan_to_num((1 / count) / np.sum(1 / count[np.isfinite(1 / count)]))
        ret = dict(zip(vals.astype(int), ret))
    else:
        raise RuntimeError("Mode must be one of {}".format(modes))

    ret_df = pd.DataFrame.from_dict(ret, orient="index", columns=[mode]).round(3)
    if verbose:
        print(ret_df)

    return ret_df.values.T[0]


def losses(loss_type: Union[str, Loss], **kwargs):
    """
    Get the loss function based on the given loss type.

    Parameters:
    - loss_type: The loss type to get the loss function for.

    Returns:
    - The loss function based on the given loss type.

    Raises:
    - LossNotYetSupportedException: If the given loss type is not yet supported.
    """
    reduction = kwargs.get("reduction", Reduction.mean.value)
    wscc_classes = kwargs.get("classes", None)
    wscc_weights = kwargs.get("wscc_weights", None)

    if isinstance(loss_type, str):
        loss_type = Loss[loss_type]

    if loss_type == Loss.CrossEntropyLoss:
        loss = CrossEntropyLoss(reduction=reduction)
    elif loss_type == Loss.FocalLoss:
        loss = FocalLoss()
    elif loss_type == Loss.DiceLoss:
        loss = DiceLoss()
    elif loss_type == Loss.NLLLoss:
        loss = NLLLoss(reduction=reduction)
    elif loss_type == Loss.TverskyLoss:
        loss = TverskyLoss()
    elif loss_type == Loss.WeightedSparseCategoricalCrossentropy:
        loss = WeightedSparseCategoricalCrossentropy(
            weights=wscc_weights, classes=wscc_classes, reduction=reduction
        )
    else:
        raise LossNotYetSupportedException(loss_type)
    return loss
