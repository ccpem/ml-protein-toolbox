# class instead of value error when giving logts and not predictions
class NotBetweenZeroAndOneError(Exception):
    """
    Exception raised when values are not between 0 and 1.
    This could be because the values are logits and not predictions.
    """

    def __init__(self):
        super().__init__(
            "Values given are not between 0 and 1. This could be because the values are logits and not predictions."
        )
