import functools

import numpy as np
import torch

from .errors import NotBetweenZeroAndOneError


def check_prediction(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        # Check if the function is a method of a class
        if isinstance(args[0], (np.ndarray, torch.Tensor)):
            data = args[0]
        else:
            data = args[1]

        if not all(0 <= val <= 1 for val in data.view(-1)):
            raise NotBetweenZeroAndOneError()

        return func(*args, **kwargs)

    return wrapper
