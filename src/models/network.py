import enum
import logging
import warnings
from abc import ABC, abstractmethod
from typing import Type

import torch


class Activation(str, enum.Enum):
    ReLU = "ReLU"
    LeakyReLU = "LeakyReLU"
    PReLU = "PReLU"
    Sigmoid = "Sigmoid"
    Tanh = "Tanh"


class Pooling(str, enum.Enum):
    MaxPool = "MaxPool"
    AvgPool = "AvgPool"


class ActivationNotYetSupportedException(Exception):
    def __init__(self, act: Activation | str):
        super().__init__(
            "Chosen activation {} is not yet supported. Please chose an activation from the list of supported activations."
            "{}".format(act, Activation.__members__)
        )


class PoolingNotYetSupportedException(Exception):
    def __init__(self, pool: Pooling | str):
        super().__init__(
            "Chosen pooling {} is not yet supported. Please chose an activation from the list of supported activations."
            "{}".format(pool, Pooling.__members__)
        )


# TODO add more supported enums, including options for padding and activations e.g. tanh, sigmoid


class Network(torch.nn.Module, ABC):
    @abstractmethod
    def __init__(
        self,
        ndim: int,
        chin: int | None = None,
        depth: int | None = None,
        activ: str | Activation | None = Activation.ReLU,
        pool: str | Pooling = Pooling.MaxPool,
    ):
        self.NDIM = ndim
        self.CH = chin
        self.DEPTH = depth

        self.CONV, self.TCONV, self.BNORM, self.ACTIV, self.POOL, self.DROP = (
            set_layer_dims(self.NDIM, activation=activ, pooling=pool)
        )

        super(Network, self).__init__()

    def __call__(self, *xs: torch.Tensor):
        if self.CONV is None:
            raise self.NetworkNotImplementedException()
        if len(xs[0].shape[2:]) != self.NDIM:
            raise self.LayerDimensionException(self.NDIM, len(xs[0].shape[2:]))
        if self.CH is not None and xs[0].shape[1] != self.CH:
            raise self.InputChannelsException(self.CH, xs[0].shape[1])
        if self.DEPTH is not None:
            check_input_pooling(xs[0].shape[2:], self.DEPTH)
        return super().__call__(*xs)  # super calls forward

    @abstractmethod
    def forward(self, x: torch.Tensor) -> torch.Tensor:
        pass

    def CONV_BLOCK(self, *args, **kwargs):
        return ConvBlock(self.NDIM, *args, **kwargs)

    def CONV_DOWN(self, *args, **kwargs):
        return ConvDown(self.NDIM, *args, **kwargs)

    def CONV_UP(self, *args, **kwargs):
        return ConvUp(self.NDIM, *args, **kwargs)

    class NetworkNotImplementedException(Exception):
        def __init__(self):
            super().__init__(
                "Dimensions of layers have not been defined. Please ensure to call "
                "superclass constructor when extending the class."
            )

    class LayerDimensionException(Exception):
        def __init__(self, dim: int, sh: int):
            super().__init__(
                "The layers have been defined for {}D input, but you're passing {}D data.".format(
                    dim, sh
                )
            )

    class InputChannelsException(Exception):
        def __init__(self, ch: int, inch: int):
            super().__init__(
                "The layers have been defined for {} channels, but you're passing \
                data with {} channels.".format(ch, inch)
            )


class DimensionNotSupportedException(Exception):
    def __init__(
        self,
        ndim: int,
    ):
        super().__init__(
            "Required data dimension {}D is not supported. Currently supported dimensions are 2D and 3D".format(
                ndim
            )
        )


def set_activation(
    activation: str | Activation,
) -> Type[
    torch.nn.ReLU
    | torch.nn.LeakyReLU
    | torch.nn.PReLU
    | torch.nn.Tanh
    | torch.nn.Sigmoid
]:
    if activation not in Activation.__members__:
        raise ActivationNotYetSupportedException(activation)
    if activation == Activation.ReLU:
        activ: Type[
            torch.nn.ReLU
            | torch.nn.LeakyReLU
            | torch.nn.PReLU
            | torch.nn.Tanh
            | torch.nn.Sigmoid
        ] = torch.nn.ReLU
    elif activation == Activation.LeakyReLU:
        activ = torch.nn.LeakyReLU
    elif activation == Activation.PReLU:
        activ = torch.nn.PReLU
    elif activation == Activation.Tanh:
        activ = torch.nn.Tanh
    elif activation == Activation.Sigmoid:
        activ = torch.nn.Sigmoid
    else:
        raise ActivationNotYetSupportedException(activation)
    return activ


def set_pooling(
    ndim: int, pooling: str | Pooling
) -> Type[
    torch.nn.MaxPool3d | torch.nn.MaxPool2d | torch.nn.AvgPool3d | torch.nn.AvgPool2d
]:
    if pooling not in Pooling.__members__:
        raise PoolingNotYetSupportedException(pooling)
    if ndim == 2:
        if pooling == Pooling.MaxPool:
            pool: Type[
                torch.nn.MaxPool3d
                | torch.nn.MaxPool2d
                | torch.nn.AvgPool3d
                | torch.nn.AvgPool2d
            ] = torch.nn.MaxPool2d
        elif pooling == Pooling.AvgPool:
            pool = torch.nn.AvgPool2d
        else:
            raise PoolingNotYetSupportedException(pooling)
    elif ndim == 3:
        if pooling == Pooling.MaxPool:
            pool = torch.nn.MaxPool3d
        elif pooling == Pooling.AvgPool:
            pool = torch.nn.AvgPool3d
        else:
            raise PoolingNotYetSupportedException(pooling)
    else:
        raise DimensionNotSupportedException(ndim)
    return pool


def set_layer_dims(
    ndim: int,
    activation: str | Activation | None = Activation.ReLU,
    pooling: str | Pooling = Pooling.MaxPool,
) -> tuple[
    Type[torch.nn.Conv2d | torch.nn.Conv3d],
    Type[torch.nn.ConvTranspose2d | torch.nn.ConvTranspose3d],
    Type[torch.nn.BatchNorm2d | torch.nn.BatchNorm3d],
    Type[
        torch.nn.ReLU
        | torch.nn.LeakyReLU
        | torch.nn.PReLU
        | torch.nn.Tanh
        | torch.nn.Sigmoid
    ]
    | None,
    Type[
        torch.nn.MaxPool3d
        | torch.nn.MaxPool2d
        | torch.nn.AvgPool3d
        | torch.nn.AvgPool2d
    ],
    Type[torch.nn.Dropout],
]:
    if ndim == 2:
        conv: Type[torch.nn.Conv2d | torch.nn.Conv3d] = torch.nn.Conv2d
        tconv: Type[torch.nn.ConvTranspose2d | torch.nn.ConvTranspose3d] = (
            torch.nn.ConvTranspose2d
        )
        bnorm: Type[torch.nn.BatchNorm2d | torch.nn.BatchNorm3d] = torch.nn.BatchNorm2d
    elif ndim == 3:
        conv = torch.nn.Conv3d
        tconv = torch.nn.ConvTranspose3d
        bnorm = torch.nn.BatchNorm3d
    else:
        logging.error("Data must be 2D or 3D.")
        exit(1)

    # separate functions allow for choice and object-specific overwrites
    activ: (
        Type[
            torch.nn.ReLU
            | torch.nn.LeakyReLU
            | torch.nn.PReLU
            | torch.nn.Tanh
            | torch.nn.Sigmoid
        ]
        | None
    ) = None
    if activation:
        activ = set_activation(activation)
    pool = set_pooling(ndim, pooling)

    drop = torch.nn.Dropout

    return conv, tconv, bnorm, activ, pool, drop


def check_input_pooling(input_size: torch.Size, depth: int):
    class DataTooSmallWarning(Warning):
        def __init__(self, shape: list[float | int]):
            super().__init__(
                "\nData passed to the model might be too small for maeningful results.\nTry decreasing the depth of the"
                " network or increasing the size of the data.\nSize after all pooling operations "
                "(downsampling): {}".format(shape)
            )

    sh = [x / (2**depth) for x in input_size]

    assert all([int(x) == x for x in sh]), (
        "Input size not compatible with --depth. Input must be divisible "
        "by {}.".format(2**depth)
    )
    if any([int(x) < 4 for x in sh]):
        warnings.warn(DataTooSmallWarning(sh))


def set_device(gpu: bool = True) -> torch.device:
    device = torch.device("cuda:0" if gpu and torch.cuda.is_available() else "cpu")
    if gpu and device == "cpu":
        logging.warning("\n\nWARNING: no GPU available, running on CPU instead.\n")
    return device


class ConvBlock(Network):
    def __init__(
        self,
        ndim: int,
        chin: int,
        chout: int,
        kernel_size: int = 3,
        stride: int = 1,
        depth: int = 1,
        padding: int = 1,
        bnorm: bool = True,
        activ: str | Activation | None = Activation.ReLU,
        pool: str | Pooling = Pooling.MaxPool,
        dropout: float = 0,
    ):
        super(ConvBlock, self).__init__(ndim, activ=activ, pool=pool)

        self.conv_block = torch.nn.Sequential()

        chprev = chin
        for d in range(depth):
            self.conv_block.append(
                self.CONV(
                    chprev,
                    chout,
                    kernel_size=kernel_size,
                    stride=stride,
                    padding=padding,
                ),
            )
            # TODO: should the order be made flexible?
            if dropout > 0:
                self.conv_block.append(self.DROP(dropout))
            if bnorm:
                self.conv_block.append(self.BNORM(chout))
            if activ is not None and self.ACTIV is not None:
                self.conv_block.append(self.ACTIV())
            chprev = chout

    def forward(self, x: torch.Tensor):
        x = self.conv_block(x)
        return x


class ConvDown(Network):
    def __init__(
        self,
        ndim: int,
        chin: int,
        chout: int,
        bnorm: bool = True,
        activ: str | Activation = Activation.ReLU,
        pool: str | Pooling = Pooling.MaxPool,
        down_first: bool = True,
    ):
        super(ConvDown, self).__init__(ndim, activ=activ, pool=pool)

        self.down = self.POOL(2, 2)
        self.double_conv = self.CONV_BLOCK(chin, chout, depth=2, bnorm=bnorm)

        self.down_first = down_first

    def forward(self, x: torch.Tensor):
        if self.down_first:
            x = self.down(x)
        x = self.double_conv(x)
        if not self.down_first:
            x = self.down(x)
        return x


class ConvUp(Network):
    def __init__(
        self,
        ndim: int,
        chin: int,
        chout: int,
        bnorm: bool = True,
        activ: str | Activation = Activation.ReLU,
        pool: str | Pooling = Pooling.MaxPool,
        up_first: bool = True,
        bilinear: bool = False,
    ):
        super(ConvUp, self).__init__(ndim, activ=activ, pool=pool)

        if not bilinear:
            self.up: (
                torch.nn.ConvTranspose2d | torch.nn.ConvTranspose3d | torch.nn.Upsample
            ) = self.TCONV(chin, chin // 2, kernel_size=2, stride=2)
        else:
            mode = "bilinear" if self.NDIM == 2 else "trilinear"
            self.up = torch.nn.Upsample(scale_factor=2, mode=mode, align_corners=True)
            self.conv = self.CONV(chin, chout, kernel_size=3, padding=1)

        self.double_conv = self.CONV_BLOCK(chin, chout, depth=2, bnorm=bnorm)

        self.up_first = up_first
        self.bilinear = bilinear

    def forward(self, x: torch.Tensor, skip: torch.Tensor | None = None):
        if self.up_first:
            x = self.up(x)
            if self.bilinear:
                x = self.conv(x)
        if skip is not None:
            x = torch.cat([x, skip], dim=1)
        x = self.double_conv(x)
        if not self.up_first:
            x = self.up(x)
            if self.bilinear:
                x = self.conv(x)
        return x


class ResBlock(Network):
    def __init__(
        self,
        ndim: int,
        chin: int,
        chout: int,
        bnorm: bool = True,
        activ: str | Activation = Activation.ReLU,
        pool: str | Pooling = Pooling.MaxPool,
    ):
        super(ResBlock, self).__init__(ndim, activ=activ, pool=pool)

        self.conv = self.CONV(chin, chout, kernel_size=1, padding=0)

        self.conv1 = self.CONV_BLOCK(chout, chout, depth=1, bnorm=bnorm, padding=1)
        # add activation separately after skip connection
        self.conv2 = self.CONV_BLOCK(
            chout, chout, depth=1, bnorm=bnorm, padding=1, activ=None
        )
        if self.ACTIV:
            self.activation = self.ACTIV()

    def forward(self, x: torch.Tensor, skip: torch.Tensor | None = None):
        residual = self.conv(x)
        # residual block
        x = self.conv1(residual)
        x = self.conv2(x)
        x += residual
        x = self.activation(x)
        return x
