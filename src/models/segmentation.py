import torch

from src.models.network import Activation, Network, Pooling


class UNet(Network):
    def __init__(
        self,
        ndim: int,
        chin: int,
        chout: int,
        depth: int = 4,
        ch: int = 64,
        bnorm: bool = True,
        activ: str | Activation = Activation.ReLU,
        pool: str | Pooling = Pooling.MaxPool,
        bilinear: bool = False,
    ):
        super(UNet, self).__init__(ndim, chin, depth=depth, activ=activ, pool=pool)

        self.input = self.CONV_BLOCK(chin, ch, depth=2, bnorm=bnorm)

        self.down = torch.nn.ModuleList()
        assert isinstance(self.DEPTH, int)  # mainly to avoid mypy error
        for d in range(self.DEPTH):
            self.down.append(self.CONV_DOWN(ch, ch * 2, bnorm=bnorm))
            ch = ch * 2

        self.up = torch.nn.ModuleList()
        for d in range(self.DEPTH):
            self.up.append(self.CONV_UP(ch, ch // 2, bnorm=bnorm, bilinear=bilinear))
            ch = ch // 2

        self.output = self.CONV(ch, chout, 3, stride=1, padding=1)

        # or otherwise:
        # self.input = ConvBlock(chin, ch, depth=2, bnorm=bnorm)
        # self.down1 = ConvDown(64, 128, bnorm=bnorm)
        # self.down2 = ConvDown(128, 256, bnorm=bnorm)
        # self.down3 = ConvDown(256, 512, bnorm=bnorm)
        # self.down4 = ConvDown(512, 1024, bnorm=bnorm)
        # self.up4 = ConvUp(1024, 512, bnorm=bnorm)
        # self.up3 = ConvUp(512, 256, bnorm=bnorm)
        # self.up2 = ConvUp(256, 128, bnorm=bnorm)
        # self.up1 = ConvUp(128, 64, bnorm=bnorm)
        # self.output = self.CONV(64, chout, 3, stride=1, padding=1)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.input(x)
        assert isinstance(self.DEPTH, int)
        skip_connections = []
        for d in range(self.DEPTH):
            skip_connections.append(x)
            x = self.down[d](x)

        for d in range(self.DEPTH):
            x = self.up[d](x, skip_connections[-(d + 1)])

        x = self.output(x)

        return x


# Usage example:
#
#
# x = torch.randn(2, 1, 64, 64, 16)
#
# c = UNet(3, 1, 2, activ=Activation.PReLU)
#
# y = c(x)
