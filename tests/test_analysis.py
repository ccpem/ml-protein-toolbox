import os
import shutil
import unittest

import mrcfile
import numpy as np

from src.analysis import predict_unet
from src.multi_proteins import load_data, preproces_data


class TestInference(unittest.TestCase):
    def setUp(self):
        if os.getcwd().split("/")[-1] != "tests":
            os.chdir("tests")

        path = "data/train_data"
        self.data = load_data(path, [2.5])
        self.data = preproces_data(self.data, mode="crop", cshape=64)

        self.pred_path = os.path.join(path, "2.5preds")
        if os.path.exists(self.pred_path):
            shutil.rmtree(self.pred_path)

        self.model = "data/train_data/model_64x64x64_epoch0039_checkpoint.h5"

    def test_predict(self):
        predict_unet(self.data, model_name=self.model)
        self.assertTrue(os.path.exists(os.path.join(self.pred_path)))
        self.assertEqual(len(os.listdir(self.pred_path)), 4)
        self.assertTrue(os.path.exists(os.path.join(self.pred_path, "5ltg_1.mrc")))
        self.assertTrue(os.path.exists(os.path.join(self.pred_path, "5ltg_2.mrc")))
        self.assertEqual(
            mrcfile.open(os.path.join(self.pred_path, "5ltg_1.mrc")).data.shape,
            (64, 64, 64),
        )
        self.assertEqual(
            len(np.unique(mrcfile.open(os.path.join(self.pred_path, "5ltg_1.mrc")))), 1
        )

    def tearDown(self):
        shutil.rmtree(self.pred_path)
