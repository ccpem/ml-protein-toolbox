import os
import unittest

import numpy as np

from src.proteins import atm_to_map, load_map, load_model, save_map


class TestPreprocessing(unittest.TestCase):
    def setUp(self):
        if os.getcwd().split("/")[-1] != "tests":
            os.chdir("tests")

        test_pdb = "data/label_data/pdbs/"
        test_mrc = "data/train_data/2.5/"
        test_lab = "data/train_data/2.5label/"
        self.pdb_bb = os.path.join(test_pdb, "amino_4.pdb")
        self.map_bb = os.path.join(test_mrc, "amino_4.mrc")

        pdbid = self.map_bb.split("/")[-1].split(".")
        pdbid = pdbid[0] + "_backbone." + pdbid[1]
        self.save_path = os.path.join(test_lab, pdbid)

    def test_load_save(self):
        # load data and generate labels
        mmap, orig, sample, cell = load_map(self.map_bb)  # load mrc file
        x, y, z, atm, res, bb, occ, bf = load_model(self.pdb_bb)  # load pdb model
        gmap, amap = atm_to_map(
            mmap, x, y, z, atm, bb, orig, sample, cell, res=2.5
        )  # generate labels
        self.assertAlmostEqual(np.sum(gmap - mmap), 0, 0)
        self.assertEqual(len(np.unique(amap)), 3)

        # save labels
        if os.path.exists(self.save_path):
            os.remove(self.save_path)
        save_map(amap, orig, cell, path=self.save_path, overwrite=True)
        self.assertTrue(os.path.exists(self.save_path))
