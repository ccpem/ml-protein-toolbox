import os
import shutil
import unittest

import numpy as np

from src.multi_proteins import gen_labs
from src.proteins import load_map


class TestGenlabs(unittest.TestCase):
    def setUp(self):
        if os.getcwd().split("/")[-1] != "tests":
            os.chdir("tests")

        self.path = "data/label_data"
        self.gauss_path = os.path.join(self.path, "gauss-maps")
        self.label_path = os.path.join(self.path, "label-maps")
        if os.path.exists(self.gauss_path):
            shutil.rmtree(self.gauss_path)
        if os.path.exists(self.label_path):
            shutil.rmtree(self.label_path)

    def test_generate(self):
        gen_labs(self.path, res=[2.5], gen_maps=False)
        self.assertTrue(os.path.exists(self.gauss_path))
        self.assertTrue(len(os.listdir(self.gauss_path + "/2.5")), 6)

        data, _, _, _ = load_map(
            self.label_path + "/backbone/1s/2.5/amino_4_backbone.mrc"
        )
        self.assertEqual(len(np.unique(data)), 3)

    def tearDown(self):
        shutil.rmtree(self.gauss_path)
        shutil.rmtree(self.label_path)
