import unittest

import torch

from src.models.network import Activation, ResBlock
from src.models.segmentation import UNet


class TestNetwork(unittest.TestCase):
    def test_unet3d(self):
        model = UNet(3, 1, 2, activ=Activation.Tanh)
        model.eval()
        assert model.output.out_channels == 2
        x = torch.randn(2, 1, 64, 64, 16)
        with torch.no_grad():
            y = model(x)
        assert y.shape == torch.Size([2, 2, 64, 64, 16])

    def test_unet3d_trilinear(self):
        model = UNet(3, 1, 2, activ=Activation.Tanh, bilinear=True)
        model.eval()
        assert model.output.out_channels == 2
        x = torch.randn(2, 1, 64, 64, 16)
        with torch.no_grad():
            y = model(x)
        assert y.shape == torch.Size([2, 2, 64, 64, 16])

    def test_resblock(self):
        block = ResBlock(3, 1, 2)
        block.eval()
        x = torch.randn(2, 1, 64, 64, 16)
        with torch.no_grad():
            y = block(x)
        assert torch.all(0 <= y)
        assert y.shape == torch.Size([2, 2, 64, 64, 16])
