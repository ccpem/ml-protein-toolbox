import os
import sys
import unittest

import torch

sys.path.append(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
)  # MyPy Not happy with this system but it's not installable so it's fine for now

from src.losses import losses  # noqa: E402


class TestLosses(unittest.TestCase):
    def setUp(self):
        if os.getcwd().split("/")[-1] != "tests":
            os.chdir("tests")

        # 5 samples, 3 classes
        # One hot encoded target
        # Class 1: [1, 0, 0]
        # Class 2: [0, 1, 0]
        # Class 3: [0, 0, 1]

        self.target_onehot = torch.tensor(
            [
                [1, 0, 0],
                [0, 1, 0],
                [0, 0, 1],
                [1, 0, 0],
                [0, 1, 0],
            ]
        )
        self.sparse_target = torch.tensor([0, 1, 2, 0, 1])
        self.logits_perfect = torch.tensor(
            [
                [10.0, -10.0, -10.0],
                [-10.0, 10.0, -10.0],
                [-10.0, -10.0, 10.0],
                [10.0, -10.0, -10.0],
                [-10.0, 10.0, -10.0],
            ]
        )
        self.logits_good = torch.tensor(
            [
                [2.0, -1.0, -1.0],
                [-1.0, 2.0, -1.0],
                [-1.0, -1.0, 2.0],
                [2.0, -1.0, -1.0],
                [-1.0, 2.0, -1.0],
            ]
        )
        self.logits_bad_high_penal = torch.tensor(
            [
                [-10.0, 10.0, 10.0],
                [10.0, -10.0, 10.0],
                [10.0, 10.0, -10.0],
                [-10.0, 10.0, 10.0],
                [10.0, -10.0, 10.0],
            ]
        )
        self.logits_bad_low_penal = torch.ones((5, 3))
        self.predictions_perfect = torch.sigmoid(self.logits_perfect)
        self.predictions_good = torch.sigmoid(self.logits_good)
        self.predictions_bad_low_penal = torch.sigmoid(self.logits_bad_low_penal)
        self.predictions_bad_high_penal = torch.sigmoid(self.logits_bad_high_penal)

    def test_CrossEntropyLoss(self):
        loss = losses("CrossEntropyLoss")
        self.assertAlmostEqual(
            loss(self.logits_perfect, self.sparse_target).item(), 0.0
        )
        self.assertAlmostEqual(
            loss(self.logits_good, self.sparse_target).item(), 0.09492, places=5
        )
        self.assertAlmostEqual(
            loss(self.logits_bad_low_penal, self.sparse_target).item(),
            1.09861,
            places=5,
        )
        self.assertAlmostEqual(
            loss(self.logits_bad_high_penal, self.sparse_target).item(),
            20.693147,
            places=5,
        )

    def test_FocalLoss(self):
        loss = losses("FocalLoss")
        self.assertAlmostEqual(
            loss(self.logits_perfect, self.sparse_target).item(), 0.0
        )
        self.assertAlmostEqual(loss(self.logits_good, self.sparse_target).item(), 0.0)
        self.assertAlmostEqual(
            loss(self.logits_bad_low_penal, self.sparse_target).item(),
            0.23925,
            places=5,
        )

        self.assertAlmostEqual(
            loss(self.logits_bad_high_penal, self.sparse_target).item(),
            5.173285,
            places=5,
        )

    def test_DiceLoss(self):
        loss = losses("DiceLoss")

        self.assertAlmostEqual(
            loss(self.predictions_perfect, self.target_onehot).item(),
            6.1e-05,
            places=5,
        )
        self.assertAlmostEqual(
            loss(self.predictions_good, self.target_onehot).item(),
            0.25092,
            places=5,
        )
        self.assertAlmostEqual(
            loss(self.predictions_bad_low_penal, self.target_onehot).item(),
            0.510158,
            places=5,
        )
        self.assertAlmostEqual(
            loss(self.predictions_bad_high_penal, self.target_onehot).item(),
            0.93747,
            places=5,
        )

    def test_TverskyLoss(self):
        loss = losses("TverskyLoss")

        self.assertAlmostEqual(
            loss(self.predictions_perfect, self.target_onehot).item(),
            5.6e-05,
            places=5,
        )
        self.assertAlmostEqual(
            loss(self.predictions_good, self.target_onehot).item(),
            0.18731,
            places=5,
        )
        self.assertAlmostEqual(
            loss(self.predictions_bad_low_penal, self.target_onehot).item(),
            0.240557,
            places=5,
        )
        self.assertAlmostEqual(
            loss(self.predictions_bad_high_penal, self.target_onehot).item(),
            0.000340,
            places=5,
        )


if __name__ == "__main__":
    unittest.main()
