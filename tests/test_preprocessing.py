import os
import shutil
import unittest

import numpy as np

from src.multi_proteins import load_data, postproces_data, preproces_data
from src.proteins import Sample, load_map


class TestPreprocessingTiles(unittest.TestCase):
    def setUp(self):
        if os.getcwd().split("/")[-1] != "tests":
            os.chdir("tests")

        source = "data/label_data/maps/2.5/amino_4.mrc"
        self.samp = Sample("3.0", source)
        self.assertEqual(self.samp.map.shape, (32, 30, 31))

        self.cshape = 17
        self.margin = 3
        self.thresh = 0.4
        self.bg = 0.999  # needed high for synthetic maps
        self.bg_low = 0.99

        self.path = "data/train_data"

    def test_decompose(self):
        # decompose dense
        self.samp.decompose(cshape=self.cshape, margin=self.margin)
        dense_rec = self.samp.no_tiles
        self.samp.tiles = None

        # decompose with high background
        self.samp.decompose(
            cshape=self.cshape,
            margin=self.margin,
            background_limit=self.bg,
            threshold=self.thresh,
        )
        thresh_rec = self.samp.no_tiles
        self.samp.tiles = None

        # decompose with lower background (more tiles missing)
        self.samp.decompose(
            cshape=self.cshape,
            margin=self.margin,
            background_limit=self.bg_low,
            threshold=self.thresh,
        )
        thresh_low_rec = self.samp.no_tiles

        self.assertGreater(dense_rec, thresh_rec)
        self.assertGreater(thresh_rec, thresh_low_rec)

    def test_recompose(self):
        # recompose map dense
        self.samp.decompose(cshape=self.cshape, margin=self.margin)
        self.samp.recompose(map=True)
        self.assertTrue((self.samp.map == self.samp.map_rec).all())
        self.assertEqual(self.samp.map.shape, self.samp.map_rec.shape)
        self.samp.tiles = None

        # recompose map with background
        self.samp.decompose(
            cshape=self.cshape,
            margin=self.margin,
            background_limit=self.bg,
            threshold=self.thresh,
        )
        self.samp.recompose(map=True)
        self.assertFalse((self.samp.map == self.samp.map_rec).all())
        self.assertEqual(self.samp.map.shape, self.samp.map_rec.shape)

    def test_preprocess_tile(self):
        # tile postprocess wrapper
        data = load_data(self.path, [2.5])
        data = preproces_data(data, mode="tile", cshape=64)

        self.assertTrue(np.max(data[0].tiles), 1.0)

        data = postproces_data(data, map=True)
        self.assertAlmostEqual(np.sum(data[0].map), np.sum(data[0].map_rec), 2)

        d = [d for d in data if d.id == "5ltg"][0]
        d.save_map()
        new_d = load_map(os.path.join(self.path, "2.5maps/5ltg.mrc"))[0]
        self.assertEqual(d.map.shape, new_d.shape)

    def tearDown(self):
        map_path = os.path.join(self.path, "2.5maps")
        if os.path.exists(map_path):
            shutil.rmtree(map_path)


class TestPreprocessingScale(unittest.TestCase):
    def setUp(self):
        if os.getcwd().split("/")[-1] != "tests":
            os.chdir("tests")

        self.path = "data/train_data"

    def test_preprocess_scale(self):
        # preprocessing scale wrapper
        data = load_data(self.path, [2.5])
        old_d = [d.map.shape for d in data if d.id == "5ltg"][0]
        data = preproces_data(data, mode="scale", cshape=64)

        self.assertTrue(np.max([d.map for d in data]), 1.0)

        new_samp = [s.header_map.samp for s in data]
        self.assertNotEqual(new_samp[0], new_samp[1])

        d = [d for d in data if d.id == "5ltg"][0]
        d.save_map()
        new_d = load_map(os.path.join(self.path, "2.5maps/5ltg.mrc"))[0]
        self.assertEqual(d.map.shape, new_d.shape)
        self.assertNotEqual(old_d, new_d.shape)

    def tearDown(self):
        map_path = os.path.join(self.path, "2.5maps")
        if os.path.exists(map_path):
            shutil.rmtree(map_path)


class TestPreprocessingCrop(unittest.TestCase):
    def setUp(self):
        if os.getcwd().split("/")[-1] != "tests":
            os.chdir("tests")

        self.path = "data/train_data"

    def test_preprocess_crop(self):
        # preprocessing crop wrapper
        data = load_data(self.path, [2.5])
        old_d = [d.map.shape for d in data if d.id == "5ltg"][0]
        data = preproces_data(data, mode="crop", cshape=64)

        self.assertTrue(np.max([d.map for d in data]), 1.0)

        new_samp = [s.header_map.samp for s in data]
        self.assertEqual(new_samp[0], new_samp[1])

        d = [d for d in data if d.id == "5ltg"][0]
        d.save_map()
        new_d = load_map(os.path.join(self.path, "2.5maps/5ltg.mrc"))[0]
        self.assertEqual(d.map.shape, new_d.shape)
        self.assertNotEqual(old_d, new_d.shape)

    def tearDown(self):
        map_path = os.path.join(self.path, "2.5maps")
        if os.path.exists(map_path):
            shutil.rmtree(map_path)
